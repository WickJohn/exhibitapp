package com.example.filexhibitsloader

import android.content.Context
import com.example.model.Exhibit
import com.example.model.ExhibitList
import com.example.model.ExhibitsLoader
import com.google.gson.Gson
import java.io.IOException
import kotlin.text.Charsets.UTF_8

class FileExhibitsLoader(private var context: Context) : ExhibitsLoader {

    override fun getExhibitList(): List<Exhibit> {
        val jsonString = getAssetsJSON("ExhibitApi.json")
        var exhibits: ExhibitList = Gson().fromJson(jsonString, ExhibitList::class.java)

        return exhibits.list

    }

    fun getAssetsJSON(fileName: String): String? {
            var json : String? = null
        try {
            val inputStrem = context.assets.open(fileName)
            val size = inputStrem.available()
            val buffer = ByteArray(size)
            inputStrem.read(buffer)
            inputStrem.close()
            json = String(buffer, UTF_8)
        } catch (e: IOException) {

        }


        return json
    }
}