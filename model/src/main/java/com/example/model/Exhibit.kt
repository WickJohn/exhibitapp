package com.example.model

import com.google.gson.annotations.SerializedName

data class Exhibit(

    @SerializedName("title")
    val title: String,

    @SerializedName("images")
    val images: Array<String> = arrayOf<String>()
)

