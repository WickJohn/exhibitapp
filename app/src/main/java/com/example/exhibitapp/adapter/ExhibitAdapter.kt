package com.example.exhibitapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.exhibitapp.R
import com.example.model.Exhibit
import kotlinx.android.synthetic.main.exhibit_item.view.*

class ExhibitAdapter(private val exhibits: List<Exhibit>) : RecyclerView.Adapter<ExhibitAdapter.ExhibitHolder>() {

    override fun onBindViewHolder(holder: ExhibitHolder, position: Int) {
        holder.bind(exhibits.get(position))
    }

    class ExhibitHolder(val view : View) : RecyclerView.ViewHolder(view) {
            fun bind(exhibit : Exhibit) {
                view.exhibit_title.text = exhibit.title
                var adapter = ViewPagerAdapter(view.context, exhibit.images)
                view.view_pager.setAdapter(adapter)


            }
    }


    override fun onCreateViewHolder(container: ViewGroup, viewtype: Int): ExhibitHolder {
        val v = LayoutInflater.from(container?.context).inflate(R.layout.exhibit_item, container, false)
        return ExhibitHolder(v)
    }

    override fun getItemCount(): Int {
    return exhibits.size
    }




}