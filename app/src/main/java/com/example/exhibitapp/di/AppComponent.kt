package com.example.exhibitapp.di

import com.example.exhibitapp.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = arrayOf(AppModule::class, FileExhibitisModule::class))
@Singleton

interface AppComponent {
    fun inject(mainActivity: MainActivity)
}