package com.example.exhibitapp.di

import android.content.Context
import com.example.exhibitapp.MainPresenter
import com.example.exhibitapp.presenter.MainPresenterImpl
import com.example.filexhibitsloader.FileExhibitsLoader
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private var context: Context) {

    @Provides
    @Singleton
    fun provideContext() : Context {
        return context
    }

    @Provides
    fun providePresenter(fileexhibitsLoader : FileExhibitsLoader) : MainPresenterImpl {
        return MainPresenterImpl(fileexhibitsLoader)
    }
}