package com.example.exhibitapp.di

import android.content.Context
import com.example.filexhibitsloader.FileExhibitsLoader
import dagger.Module
import dagger.Provides

@Module
class FileExhibitisModule {

    @Provides
    fun provideFileExhibitsApi(context: Context) : FileExhibitsLoader {
        return FileExhibitsLoader(context)
    }



}
