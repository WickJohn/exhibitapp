package com.example.exhibitapp

import com.example.model.Exhibit

interface MainView {
    fun showData( exhibits : List<Exhibit>)
}

interface MainPresenter<T : MainView> {
    var view : T?

    fun attachView(view : T) {
        this.view = view
    }

    fun detachView() {
        this.view = null
    }

    fun loadData()
}