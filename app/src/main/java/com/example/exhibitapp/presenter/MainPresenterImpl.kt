package com.example.exhibitapp.presenter

import com.example.exhibitapp.MainActivity
import com.example.exhibitapp.MainPresenter
import com.example.filexhibitsloader.FileExhibitsLoader
import com.example.model.Exhibit

class MainPresenterImpl( private val fileExhibitionLoader : FileExhibitsLoader) : MainPresenter<MainActivity> {

    override var view: MainActivity? = null


    override fun loadData()  {
        var list : List<Exhibit> = fileExhibitionLoader.getExhibitList()
        view?.showData(list)
    }
}