package com.example.exhibitapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.exhibitapp.adapter.ExhibitAdapter
import com.example.exhibitapp.di.AppComponent
import com.example.exhibitapp.di.AppModule
import com.example.exhibitapp.di.DaggerAppComponent
import com.example.exhibitapp.di.FileExhibitisModule
import com.example.exhibitapp.presenter.MainPresenterImpl
import com.example.model.Exhibit
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() , MainView {




    @Inject
     lateinit var  presenter : MainPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        presenter.attachView(this)

        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.loadData()
    }

    override fun showData(exhibits: List<Exhibit>) {
        recycler_view.adapter = ExhibitAdapter(exhibits)
    }




    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .fileExhibitisModule(FileExhibitisModule())
                .build()
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}